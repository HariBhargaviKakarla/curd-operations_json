import json
def writing(new_data, filename='sample.json'):
    with open(filename,'r+') as file:
        file_data = json.load(file)
        file_data["emp_details"].append(new_data)
        file.seek(0)
        json.dump(file_data, file, indent = 4)
 
newdata = {
		"name": "vinay",
    "id": 10980,
    "phonenumber": "9704195615"
    }
     
writing(newdata)